//
//  VerticalSlider.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 15..
//  Copyright © 2016년 FITU. All rights reserved.
//


import UIKit

@IBDesignable
public class NapySlider: UIControl {
    
    // internal variables, our views
    internal var backgroundView: UIView!
    internal var sliderView: UIView!
    internal var sliderBackgroundView: UIView!
    internal var sliderFillView: UIView!
    internal var handleView: UIView!
    
    internal var isFloatingPoint: Bool {
        get { return step % 1 != 0 ? true : false }
    }
    
    // public variables
    var titleHeight: CGFloat = 0
    var sliderWidth: CGFloat = 2
    var handleHeight: CGFloat = 5
    var handleWidth: CGFloat = 15
    
    // public inspectable variables
    
    @IBInspectable var min: Double = 0
    
    @IBInspectable var max: Double = 10
    
    @IBInspectable var step: Double = 1
    
    // colors
    @IBInspectable var handleColor: UIColor = UIColor.grayColor()
    @IBInspectable var mainBackgroundColor: UIColor = UIColor.groupTableViewBackgroundColor()
    @IBInspectable var sliderUnselectedColor: UIColor = UIColor.lightGrayColor()
    
    /**
     the position of the handle. The handle moves animated when setting the variable
     */
    var handlePosition:Double {
        set (newHandlePosition) {
            moveHandleToPosition(newHandlePosition, animated: true)
        }
        get {
            let currentY = handleView.frame.origin.y + handleHeight/2
            let positionFromMin = -(Double(currentY) - minPosition - stepheight/2) / stepheight
            
            // add an offset if slider should go to a negative value
            var stepOffset:Double = 0
            if min < 0 {
                let zeroPosition = (0 - min)/Double(step) + 0.5
                if positionFromMin < zeroPosition {
                    stepOffset = 0 - step
                }
            }
            
            //            let position = Int((positionFromMin * step + min + stepOffset) / step) * Int(step)
            let position = Double(Int((positionFromMin * step + min + stepOffset) / step)) * step
            return Double(position)
        }
    }
    
    var disabled:Bool = false {
        didSet {
            sliderBackgroundView.alpha = disabled ? 0.4 : 1.0
            self.userInteractionEnabled = !disabled
        }
    }
    
    
    private var steps: Int {
        get {
            if (min == max || step == 0) {
                return 1
            } else {
                return Int(round((max - min) / step)) + 1
            }
        }
    }
    
    private var maxPosition:Double {
        get {
            return 0
        }
    }
    
    private var minPosition:Double {
        get {
            return Double(sliderView.frame.height)
        }
    }
    
    
    private var stepheight:Double {
        get {
            return (minPosition - maxPosition) / Double(steps - 1)
        }
    }
    
    var delegate: VerticalSliderDelegate!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    private func setup() {
        backgroundView = UIView()
        backgroundView.userInteractionEnabled = false
        addSubview(backgroundView)
        
        sliderBackgroundView = UIView()
        sliderBackgroundView.userInteractionEnabled = false
        backgroundView.addSubview(sliderBackgroundView)
        
        sliderFillView = UIView()
        sliderFillView.userInteractionEnabled = false
        sliderBackgroundView.addSubview(sliderFillView)
        
        sliderView = UIView()
        sliderView.userInteractionEnabled = false
        sliderBackgroundView.addSubview(sliderView)
        
        handleView = UIView()
        handleView.userInteractionEnabled = false
        sliderView.addSubview(handleView)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        let sliderPaddingTop:CGFloat = 0
        let sliderPaddingBottom:CGFloat = 0
        
        backgroundView.frame = CGRectMake(0, titleHeight, frame.size.width, frame.size.height - titleHeight)
        backgroundView.backgroundColor = mainBackgroundColor
        
        sliderBackgroundView.frame = CGRectMake(backgroundView.frame.width/2 - sliderWidth/2, sliderPaddingTop, sliderWidth, backgroundView.frame.height - (sliderPaddingTop + sliderPaddingBottom))
        sliderBackgroundView.backgroundColor = sliderUnselectedColor
        
        sliderView.frame = CGRectMake(0, sliderWidth/2, sliderBackgroundView.frame.width, sliderBackgroundView.frame.height - sliderWidth)
        sliderView.backgroundColor = UIColor.clearColor()
        
        handleView.frame = CGRectMake(-(handleWidth-sliderWidth)/2, sliderView.frame.height/2 - handleHeight/2, handleWidth, handleHeight)
        handleView.backgroundColor = handleColor
        
        sliderFillView.frame = CGRectMake(0, handleView.frame.origin.y + handleHeight, sliderBackgroundView.frame.width, sliderBackgroundView.frame.height-handleView.frame.origin.y - handleHeight)
        sliderFillView.backgroundColor = tintColor
    }
    
    public override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        super.beginTrackingWithTouch(touch, withEvent: event)
        
        return true
    }
    
    public override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        super.continueTrackingWithTouch(touch, withEvent: event)
        let _ = handlePosition
        let point = touch.locationInView(sliderView)
        moveHandleToPoint(point)
        
        return true
    }
    
    public override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
        super.endTrackingWithTouch(touch, withEvent: event)
        
        let endPosition = handlePosition
        handlePosition = endPosition
        
    }
    
    public override func cancelTrackingWithEvent(event: UIEvent?) {
        super.cancelTrackingWithEvent(event)
    }
    
    
    private func moveHandleToPoint(point:CGPoint) {
        var newY:CGFloat
        
        newY = point.y - CGFloat(handleView.frame.height/2)
        
        if newY < -handleHeight/2 {
            newY = -handleHeight/2
        } else if newY > sliderView.frame.height - handleHeight/2 {
            newY = sliderView.frame.height - handleHeight/2
        }
        
        handleView.frame.origin.y = CGFloat(newY)
        sliderFillView.frame = CGRectMake(0 , CGFloat(newY) + handleHeight, sliderBackgroundView.frame.width, sliderBackgroundView.frame.height-handleView.frame.origin.y - handleHeight)
        
    }
    
    private func moveHandleToPosition(position:Double, animated:Bool = false) {
        if step == 0 { return }
        
        var goPosition = position
        
        if position >= max { goPosition = max }
        if position <= min { goPosition = min }
        
        let positionFromMin = (goPosition - min) / step
        
        let newY = CGFloat(minPosition - positionFromMin * stepheight)
        
        if animated {
            UIView.animateWithDuration(0.3, animations: {
                self.handleView.frame.origin.y = newY - self.handleHeight/2
                self.sliderFillView.frame = CGRectMake(0 , CGFloat(newY) + self.handleHeight/2, self.sliderBackgroundView.frame.width, self.sliderBackgroundView.frame.height - self.handleView.frame.origin.y - self.handleHeight)
            })
        } else {
            self.handleView.frame.origin.y = newY - self.handleHeight/2
            self.sliderFillView.frame.origin.y = CGFloat(newY) + self.handleHeight
        }
        
        if delegate != nil {
            delegate.handleMoved(position)
        }
        
    }
}

protocol VerticalSliderDelegate {
    func handleMoved(position: Double)
}
