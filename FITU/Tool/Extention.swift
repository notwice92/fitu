//
//  Extention.swift
//  FITU
//
//  Created by 조경래 on 2016. 5. 15..
//  Copyright © 2016년 FITU. All rights reserved.
//

import Foundation
import UIKit

extension String{
    var localized:String{
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
    func networkStringDate() -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.dateFromString(self)!
    }
}
extension NSDate{
    func yearString() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.stringFromDate(self)
    }
    func monthString() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM"
        return "\(Int.init(dateFormatter.stringFromDate(self))! as Int)"
    }
    func dayString() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd"
        return "\(Int.init(dateFormatter.stringFromDate(self))! as Int)"
    }
    func networkDateString() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        return dateFormatter.stringFromDate(self)
    }
}