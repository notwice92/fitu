//
//  FITInBodyView.swift
//  FITU
//
//  Created by Yenos on 2016. 7. 8..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit
import MKRingProgressView
import TWRCharts
//simport PROG
class FITInBodyView: UIView {
    
    @IBOutlet weak var progressGroup: MKRingProgressGroupView!
//    @IBOutlet weak var barGraph: TWRChartView!

    @IBOutlet weak var viewBarBack: UIView!
let graphView = ScrollableGraphView()
    let graphView2 = ScrollableGraphView()
    override init(frame: CGRect) {
        
        super.init(frame:frame)
        
            }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("calling")
        
    }
//    func initBarChart(){
////        var explicitArray_1: [Int] = [10,20,5,15,4]
////        var explicitArray_2: [Int] = [10,20,5,15,4]
////        var dataSet1:TWRDataSet(initData explicitArray_1)
//        
//        
//     
////        barGraph.loadBarChart(line)
//        
//        
////        TWRDataSet *dataSet1 = [[TWRDataSet alloc] initWithDataPoints:@[@10, @15, @5, @15, @5]];
////        TWRDataSet *dataSet2 = [[TWRDataSet alloc] initWithDataPoints:@[@5, @10, @5, @15, @10]];
////        
////        NSArray *labels = @[@"A", @"B", @"C", @"D", @"E"];
////        
////        // Instantiate the chart object
////        TWRLineChart *line = [[TWRLineChart alloc] initWithLabels:labels
////        dataSets:@[dataSet1, dataSet2]
////        animated:NO];
////        
////        // Load the chart object onto the view
////        [_chartView loadLineChart:line];
//
//    }
    func initBar(){
        print("init bar")
        
        let data: [Double] = [4, 0, 15, 0, 23, 0,23,0,6,0,20,0,10,0,20,3,10,0,30,0]
        let labels = ["one", "", "two", "", "three", "", "four", "", "five","","six","","seven","","seven","","seven","","seven","","seven","","seven","","seven",""]
        
        let data2: [Double] = [0, 8, 0, 16, 0, 42,0,4,0,7,0,30,0,3,0,30,0,20,0,10]
        
        graphView.setData(data, withLabels: labels)
        
        // Disable the lines and data points.
        graphView.shouldDrawDataPoint = false
        graphView.lineColor = UIColor.clearColor()
        graphView.backgroundColor = UIColor.clearColor()
        
        // Tell the graph it should draw the bar layer instead.
        graphView.shouldDrawBarLayer = true
        
        // Customise the bar.
        graphView.barWidth = 10
        
        
        graphView.barLineWidth = 1
        graphView.barLineColor = UIColor.colorFromHex("#ffffff")
        graphView.barColor = UIColor.blackColor()
        graphView.dataPointLabelColor = UIColor.colorFromHex("#D8D8D8")
        graphView.backgroundFillColor = UIColor.clearColor()
        
        graphView.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
        graphView.referenceLineColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        graphView.referenceLineLabelColor = UIColor.whiteColor()
        graphView.numberOfIntermediateReferenceLines = 5
        graphView.dataPointLabelColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        
        graphView.shouldAnimateOnStartup = true
        graphView.shouldAdaptRange = true
        graphView.adaptAnimationType = ScrollableGraphViewAnimationType.Elastic
        graphView.animationDuration = 1.5
        graphView.rangeMax = 50
        graphView.shouldRangeAlwaysStartAtZero = true
        
        
        
        graphView2.setData(data2, withLabels: labels)
        
        // Disable the lines and data points.
        graphView2.shouldDrawDataPoint = false
        graphView2.lineColor = UIColor.clearColor()
        graphView.backgroundColor = UIColor.clearColor()
        // Tell the graph it should draw the bar layer instead.
        graphView2.shouldDrawBarLayer = true
        
        // Customise the bar.
        graphView2.barWidth = 10
        
        graphView2.barLineWidth = 1
        graphView2.barLineColor = UIColor.colorFromHex("#ffffff")
        graphView2.barColor = UIColor.colorFromHex("#D8D8D8")
        graphView2.dataPointLabelColor = UIColor.colorFromHex("#D8D8D8")
        graphView2.backgroundFillColor = UIColor.clearColor()
        
        graphView2.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
        graphView2.numberOfIntermediateReferenceLines = 5

        
        graphView2.shouldAnimateOnStartup = true
        graphView2.shouldAdaptRange = true
        graphView2.adaptAnimationType = ScrollableGraphViewAnimationType.Elastic
        graphView2.animationDuration = 1.5
        graphView2.rangeMax = 50
        graphView2.shouldRangeAlwaysStartAtZero = true
        
        graphView2.secondBarView = graphView
        graphView2.referenceLineLabelColor = UIColor.grayColor()
        graphView2.dataPointLabelColor = UIColor.grayColor()
        graphView2.referenceLineColor = UIColor.grayColor()

        viewBarBack.addSubview(graphView)
        viewBarBack.addSubview(graphView2)
        
        graphView.translatesAutoresizingMaskIntoConstraints = false
        graphView2.translatesAutoresizingMaskIntoConstraints = false
        
        //기본 제약사항 코드로
        Utilz.defaultConstraints(graphView, backView:viewBarBack)
        Utilz.defaultConstraints(graphView2, backView:viewBarBack)
        
    }
    
    func initRound(){
        print("init ROund")
        
//        progressGroup.clearsContextBeforeDrawing = true
        
        progressGroup.ring1.startColor = UIColor.colorFromHex("#D8D8D8")
        progressGroup.ring1.endColor =  UIColor.colorFromHex("#D8D8D8")
        progressGroup.ring1.ringWidth = 15
        progressGroup.ring1.progress = 0.0
        progressGroup.ring1.backgroundRingColor = UIColor.colorFromHex("#ffffff")
        
        progressGroup.ring2.startColor = UIColor.blackColor()
        progressGroup.ring2.endColor = UIColor.blackColor()
        progressGroup.ring2.ringWidth = 15
        progressGroup.ring2.progress = 0.0
        progressGroup.ring2.backgroundRingColor = UIColor.colorFromHex("#ffffff")
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(3.0)
        print("anim!!")
        progressGroup.ring1.progress = 0.7
        progressGroup.ring2.progress = 0.3
        CATransaction.commit()
        
//        progressGroup.ring3.hidden  = true
    }

}
