//
//  Utilz.swift
//  FITU
//
//  Created by Yenos on 2016. 5. 22..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

let screenSize: CGRect = UIScreen.mainScreen().bounds
let SCREEN_WIDTH = screenSize.width
let SCREEN_HEIGHT = screenSize.height

class Utilz: NSObject {

    //백뷰에 가득차도록 제약두기.
   static func defaultConstraints(meView: UIView, backView: UIView){
    
        //Trallking
        let filterTrailling = NSLayoutConstraint(item: meView, attribute: NSLayoutAttribute.TrailingMargin, relatedBy: NSLayoutRelation.Equal, toItem: backView, attribute: NSLayoutAttribute.TrailingMargin, multiplier: 1, constant: 0)
        //         top constraint
        //top
        let filterTop = NSLayoutConstraint(item: meView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem:backView, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
        // bottom constraint
        let filterBottom = NSLayoutConstraint(item: meView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem:backView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant:0)
        // leading margin constraint
        let filterLeading = NSLayoutConstraint(item: meView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem:backView, attribute: NSLayoutAttribute.LeadingMargin, multiplier: 1, constant: -10)
        
        NSLayoutConstraint.activateConstraints([filterTrailling, filterTop, filterBottom, filterLeading])
    
    }

}
