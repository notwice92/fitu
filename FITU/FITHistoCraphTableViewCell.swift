//
//  FITHistoCraphTableViewCell.swift
//  FITU
//
//  Created by Yenos on 2016. 5. 22..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class FITHistoCraphTableViewCell: UITableViewCell {

    @IBOutlet weak var viewGraph: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()

        let graphView = GraphView(frame: CGRectMake(0,0,SCREEN_WIDTH*0.86 ,viewGraph.frame.height+5))
        let data = [4.0, 8.0, 15.0, 16.0, 23.0, 42.0]
        let labels = ["one", "two", "three", "four", "five", "six"]
        graphView.backgroundFillColor = UIColor.colorFromHex("#333333")
        
        graphView.rangeMax = 50
        
        graphView.lineWidth = 1
        graphView.lineColor = UIColor.colorFromHex("#777777")
        graphView.lineStyle = GraphViewLineStyle.Smooth
        
        graphView.shouldFill = true
        graphView.fillType = GraphViewFillType.Gradient
        graphView.fillColor = UIColor.colorFromHex("#555555")
        graphView.fillGradientType = GraphViewGradientType.Linear
        graphView.fillGradientStartColor = UIColor.colorFromHex("#555555")
        graphView.fillGradientEndColor = UIColor.colorFromHex("#444444")
        
        graphView.dataPointSpacing = 80
        graphView.dataPointSize = 2
        graphView.dataPointFillColor = UIColor.whiteColor()
        
        graphView.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
        graphView.referenceLineColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        graphView.referenceLineLabelColor = UIColor.whiteColor()
        graphView.dataPointLabelColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)

        graphView.setData(data, withLabels: labels)
        viewGraph.addSubview(graphView)


    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
