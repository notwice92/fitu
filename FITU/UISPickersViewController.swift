//
//  UISPickersViewController.swift
//  FITU
//
//  Created by Yenos on 2016. 7. 9..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class UISPickersViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate  {
//    @IBOutlet weak var picekerView: !
    @IBOutlet weak var picekerView: UIPickerView!
    var arrPickerData = ["one", "two", "three", "seven", "fifteen"]

    override func viewDidLoad() {
        super.viewDidLoad()
        print("picker loaddd")
        
        picekerView.delegate = self
        picekerView.dataSource = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("selected ==> ",arrPickerData[row]);
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = arrPickerData[row]
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 32) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
