//
//  MyMenuViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 5. 22..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class MyMenuViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBAction func gotoCalendar(sender: AnyObject) {
    }
    
    @IBAction func gotoClass(sender: AnyObject) {
    }
    
    @IBAction func gotoMemberManage(sender: AnyObject) {
    }
  
    @IBAction func changeToMemeber(sender: AnyObject) {
    }
    
    @IBAction func gotoSetting(sender: AnyObject) {
    }
    
    @IBAction func logout(sender: AnyObject) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
        self.profileImageView.clipsToBounds = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
