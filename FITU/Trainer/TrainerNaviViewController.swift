//
//  TrainerNaviViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 5. 15..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit
import ENSwiftSideMenu

class TrainerNaviViewController: ENSideMenuNavigationController, ENSideMenuDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let comST = UIStoryboard(name: "Trainer", bundle: nil)
        let controller = comST.instantiateViewControllerWithIdentifier("MyMenuViewController") as! MyMenuViewController
        
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: controller, menuPosition: .Left)
        //sideMenu?.delegate = self //optional
        sideMenu?.menuWidth = 200.0 // optional, default is 160
        //sideMenu?.bouncingEnabled = false
        //sideMenu?.allowPanGesture = false
        // make navigation bar showing over side menu
        view.bringSubviewToFront(navigationBar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
