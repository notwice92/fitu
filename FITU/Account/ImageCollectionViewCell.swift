//
//  ImageCollectionViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 5..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview: UIImageView!
}
