//
//  RegisterViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 17..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickProfileSetting(sender: AnyObject) {
        let registerVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingProfileViewController") as! SettingProfileViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    
}
