//
//  SettingProfileViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 17..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class SettingProfileViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var genderInputView: UITextField!
    @IBOutlet weak var birthInputView: UITextField!
    
    @IBAction func onClickDone(sender: AnyObject) {
        self.appDelegate.goToCommonMain()
    }
    
    var gender = ["남", "여"]
    var genderPicker = UIPickerView()
    
    var datePickerView = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //성별
        self.genderPicker.delegate = self
        self.genderPicker.dataSource = self
        self.genderInputView.inputView = self.genderPicker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(SettingProfileViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        
        self.genderInputView.inputAccessoryView = toolBar
        
        //생년월일
        self.datePickerView.datePickerMode = UIDatePickerMode.Date
        self.datePickerView.addTarget(self, action: #selector(SettingProfileViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.birthInputView.inputView = datePickerView
        self.birthInputView.inputAccessoryView = toolBar
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yy.MM.dd"
        birthInputView.text = dateFormatter.stringFromDate(sender.date)
    }
    
    func donePicker() {
        genderInputView.resignFirstResponder()
        birthInputView.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gender.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.genderInputView.text = gender[row]
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gender[row]
    }
    
}
