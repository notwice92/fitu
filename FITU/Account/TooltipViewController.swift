//
//  TooltipViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 5..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class TooltipViewController: UIViewController, UICollectionViewDataSource {

    let items: [(image: String, label: String)] = [
        ("1", "The first image from space"),
        ("2", "The second image from space"),
        ("3", "The third image from space")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCollectionViewCell", forIndexPath: indexPath) as! ImageCollectionViewCell
        let item = items[indexPath.item]
        cell.imageview.image = UIImage(named: item.image)
        return cell
    }

    
    @IBAction func onClickLogin(sender: AnyObject) {
        let loginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
    @IBAction func onClickRegister(sender: AnyObject) {
        let registerVC = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
}
