//
//  FITStrengthView.swift
//  FITU
//
//  Created by Yenos on 2016. 7. 10..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class FITStrengthView: UIView {

    @IBOutlet weak var firstHeigh: NSLayoutConstraint!
    @IBOutlet weak var thirdHeigh: NSLayoutConstraint!
    @IBOutlet weak var secHeigh: NSLayoutConstraint!
//    @IBOutlet weak var thirdHigh: NSLayoutConstraint!
//    @IBOutlet weak var thirdHigh: NSLayoutConstraint!
//    @IBOutlet weak var secHigh: NSLayoutConstraint!
//    @IBOutlet weak var firstHigh: NSLayoutConstraint!
    @IBOutlet weak var viewWaveBack: UIView!
    
    let graphView = ScrollableGraphView()
    let graphView2 = ScrollableGraphView()

    
    override init(frame: CGRect) {
        
        super.init(frame:frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("calling")
        
        
    }
    func drawLine(){
        UIView.animateWithDuration(0.5, animations: {
            
            self.firstHeigh.constant = 150
            self.secHeigh.constant = 75
//            self.secHigh.constant = 75
            self.thirdHeigh.constant = 10
            
        })

        
    }
    
    func initWave(){
        print("init bar")
        
        let data: [Double] = [4, 0, 15, 0, 23, 0,23,0,6,0,20,0,10,0,20,3,10,0,30,0]
        let labels = ["one", "", "two", "", "three", "", "four", "", "five","","six","","seven","","seven","","seven","","seven","","seven","","seven","","seven",""]
        
        let data2: [Double] = [0, 8, 0, 16, 0, 42,0,4,0,7,0,30,0,3,0,30,0,20,0,10]
        
        graphView.setData(data, withLabels: labels)
        
        graphView.backgroundFillColor = UIColor.clearColor()
        graphView.backgroundColor = UIColor.clearColor()

        graphView.rangeMax = 50
        
        graphView.lineWidth = 1
        graphView.lineColor = UIColor.colorFromHex("#BDD7D4")
        graphView.lineStyle = ScrollableGraphViewLineStyle.Smooth
        
        graphView.shouldFill = true
        graphView.fillType = ScrollableGraphViewFillType.Gradient
        graphView.fillColor = UIColor.colorFromHex("#555555")
        graphView.fillGradientType = ScrollableGraphViewGradientType.Linear
        graphView.fillGradientStartColor = UIColor.colorFromHex("#BDD7D4")
        graphView.fillGradientEndColor = UIColor.colorFromHex("#BDD7D4")
//        BDD7D4
        graphView.dataPointSpacing = 80
        graphView.dataPointSize = 2
        graphView.dataPointFillColor = UIColor.whiteColor()
        
        graphView.referenceLineLabelFont = UIFont.boldSystemFontOfSize(0)
        graphView.referenceLineColor = UIColor.colorFromHex("#D8D8D8")
        graphView.referenceLineLabelColor = UIColor.colorFromHex("#D8D8D8")
        graphView.dataPointLabelColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        
        
        
        graphView2.setData(data2, withLabels: labels)
        graphView2.backgroundFillColor = UIColor.clearColor()
        graphView.backgroundColor = UIColor.clearColor()
        graphView2.rangeMax = 50
        
        graphView2.lineWidth = 1
        graphView2.lineColor = UIColor.colorFromHex("#D8D8D8")
        graphView2.lineStyle = ScrollableGraphViewLineStyle.Smooth
        
        graphView2.shouldFill = true
        graphView2.fillType = ScrollableGraphViewFillType.Gradient
        graphView2.fillColor = UIColor.colorFromHex("#555555")
        graphView2.fillGradientType = ScrollableGraphViewGradientType.Linear
        graphView2.fillGradientStartColor = UIColor.colorFromHex("#D8D8D8")
        graphView2.fillGradientEndColor = UIColor.colorFromHex("#D8D8D8")
        
        graphView2.dataPointSpacing = 80
        graphView2.dataPointSize = 2
        graphView2.dataPointFillColor = UIColor.whiteColor()
        
        graphView2.referenceLineLabelFont = UIFont.boldSystemFontOfSize(0)
        graphView2.referenceLineColor = UIColor.colorFromHex("#D8D8D8")
        graphView2.referenceLineLabelColor = UIColor.colorFromHex("#D8D8D8")
        graphView2.dataPointLabelColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        

        
        
        
//        graphView2.setData(data2, withLabels: labels)
//        
//        // Disable the lines and data points.
//        graphView2.shouldDrawDataPoint = false
//        graphView2.lineColor = UIColor.clearColor()
//        graphView.backgroundColor = UIColor.clearColor()
//        // Tell the graph it should draw the bar layer instead.
//        graphView2.shouldDrawBarLayer = true
//        
//        // Customise the bar.
//        graphView2.barWidth = 10
//        
//        graphView2.barLineWidth = 1
//        graphView2.barLineColor = UIColor.colorFromHex("#ffffff")
//        graphView2.barColor = UIColor.colorFromHex("#D8D8D8")
//        graphView2.dataPointLabelColor = UIColor.colorFromHex("#D8D8D8")
//        graphView2.backgroundFillColor = UIColor.clearColor()
//        
//        graphView2.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
//        graphView2.numberOfIntermediateReferenceLines = 5
//        
//        
//        graphView2.shouldAnimateOnStartup = true
//        graphView2.shouldAdaptRange = true
//        graphView2.adaptAnimationType = ScrollableGraphViewAnimationType.Elastic
//        graphView2.animationDuration = 1.5
//        graphView2.rangeMax = 50
//        graphView2.shouldRangeAlwaysStartAtZero = true
//        
//        graphView2.secondBarView = graphView
//        graphView2.referenceLineLabelColor = UIColor.grayColor()
//        graphView2.dataPointLabelColor = UIColor.grayColor()
//        graphView2.referenceLineColor = UIColor.grayColor()
        
        
        graphView2.secondBarView = graphView

        
        viewWaveBack.addSubview(graphView)
        viewWaveBack.addSubview(graphView2)
        
        graphView.translatesAutoresizingMaskIntoConstraints = false
        graphView2.translatesAutoresizingMaskIntoConstraints = false
        
        //기본 제약사항 코드로
        Utilz.defaultConstraints(graphView, backView:viewWaveBack)
        Utilz.defaultConstraints(graphView2, backView:viewWaveBack)
        
    }

    

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
