//
//  FITHistoListTableViewCell.swift
//  FITU
//
//  Created by Yenos on 2016. 5. 22..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class FITHistoListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelExeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
