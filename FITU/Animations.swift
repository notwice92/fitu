//
//  Animations.swift
//  FITU
//
//  Created by Yenos on 2016. 7. 10..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class Animations: NSObject {
    static func showAnimations(views: UIView){
        views.transform = CGAffineTransformMakeScale(0.01, 0.01);
        
        UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseOut, animations: { () -> Void in
            // animate it to the identity transform (100% scale)
            views.transform = CGAffineTransformIdentity;
            
        }) { (finished) -> Void in
            // if you want to do something once the animation finishes, put it here
        }
        
    }
}
