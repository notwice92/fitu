
//
//  FITRecordViewController.swift
//  FITU
//
//  Created by Yenos on 2016. 7. 9..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class FITRecordViewController: UIViewController {
    var actionSheetVC:UIAlertController!
    var controller:FITPickerViewController!
    var arrPickerData=[String]()

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelWeigh: UILabel!
    @IBOutlet weak var labelBorn: UILabel!
    @IBOutlet weak var labelFat: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let comST = UIStoryboard(name: "Main", bundle: nil)
        //         controller = comST.instantiateViewControllerWithIdentifier("UISPickersViewController") as! UISPickersViewController
        
        //        controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("UISPickersViewController") as! UISPickersViewController
        
        controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FITPickerViewController") as! FITPickerViewController
        
//        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FITRecordViewController.getPickerData(_:)), name: "NOTI_PICKER_END", object: nil)

        
    }
    func getPickerData(notification: NSNotification) {
        var strDate:String = ""
        var strWeigh:String = ""
        var strBorn:String = ""
        var strFat:String = ""
        
        if let info = notification.userInfo as? Dictionary<String,String> {
            // Check if value present before using it
            if let s = info["type"] {
                if(s=="DATE"){
                    strDate = info["resultYear"]! + "/" + info["resultMonth"]! + "/" + info["resultDay"]!
                    labelDate.text = strDate
                }else if(s=="WEIGH"){
                    strWeigh = info["resultKG"]!
                    labelWeigh.text = strWeigh
                }else if(s=="BORN"){
                    strBorn = info["resultKG"]!
                    labelBorn.text = strBorn
                }else if(s=="FAT"){
                    strFat = info["resultKG"]!
                    labelFat.text = strFat
                }
//                print("strDate = \(strDate)")
//                
//                print("strDate = \(info["resultKG"])")
                
            }
            else {
                print("no value for key\n")
            }
            
            
        }
        else {
            print("wrong userInfo type")
        }

    }
    
    
    
    @IBAction func pressComplete(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {});
     
    }
    
    
    @IBAction func pressDate(sender: AnyObject) {
        print("pree Date")
        
        controller.isDate = true
        controller.types = "DATE"
        showActionSheet()
    }
    @IBAction func pressWeigh(sender: AnyObject) {
        
        getKg()
        controller.arrPickerData = arrPickerData
        controller.isDate = false
        controller.types = "WEIGH"
        showActionSheet()

    }
    
    @IBAction func pressBorn(sender: AnyObject) {
        
        getKg()
        controller.arrPickerData = arrPickerData
        controller.isDate = false
        controller.types = "BORN"
        showActionSheet()
    }
    @IBAction func pressFat(sender: AnyObject) {
        
        getKg()
        controller.arrPickerData = arrPickerData
        controller.isDate = false
        controller.types = "FAT"
        showActionSheet()
    }
    func getKg(){
        for i in 50...120 {
            
            arrPickerData.append(String(i)+"kg")
        }
    }
    
    func showActionSheet(){
        //
        actionSheetVC = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let rect = CGRectMake(-20, 0, self.view.bounds.size.width+10 , 300.0)
        let actionSheetBackView = UIView(frame: rect)
        actionSheetBackView.backgroundColor = UIColor.greenColor()
        actionSheetBackView.addSubview(controller.view)
        
        actionSheetVC.view.addSubview(actionSheetBackView)
        controller.setActionSheet(actionSheetVC)
        self.presentViewController(actionSheetVC, animated: true, completion:{})
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        Utilz.defaultConstraints(controller.view, backView: actionSheetBackView)
    }
    
}
