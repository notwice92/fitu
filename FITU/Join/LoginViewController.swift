//
//  LoginViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 5. 15..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit
import BEMCheckBox
import Alamofire
import JSON

class LoginViewController: UIViewController {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var textfield_id: UITextField!
    
    @IBOutlet weak var textfield_pw: UITextField!
    
    @IBOutlet weak var btn_login: UIButton!
    
    @IBAction func onClickLogin(sender: AnyObject) {
        Alamofire.request(.POST, NetDefine.login, parameters: ["member_email": textfield_id.text!, "member_pwd":textfield_pw.text!])
            .validate()
            .responseData(completionHandler: { response in
                switch response.result {
                case .Success:
                    do {
                        let JSON = try response.result.value?.toJSON() as? [String : AnyObject]
                        if JSON!["confirm_message"] as! String == "True" {
                            
                            self.appDelegate.userId = JSON!["member_email"] as! String
                            self.appDelegate.userName = JSON!["member_name"] as! String
                            
                            self.appDelegate.goToCommonMain()
                        }else {
                            print("FAIL")
                        }
                        print(JSON)
                    } catch {
                        // Handle error
                    }
                case .Failure(let error):
                    print(error)
                }
            })
        }
    
    @IBOutlet weak var label_find_account: UILabel!
    
    @IBOutlet weak var auto_login_checkbox: BEMCheckBox!
    
    @IBOutlet weak var auto_login_label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textfield_id.placeholder = "ID"
        textfield_pw.placeholder = "PassWord"
        
        label_find_account.attributedText = NSAttributedString(string: "find_password_message".localized, attributes: [NSUnderlineStyleAttributeName : NSUnderlineStyle.StyleSingle.rawValue])
        let findGesture = UITapGestureRecognizer(target: self, action: #selector(self.onClickFindAccount))
        label_find_account.addGestureRecognizer(findGesture)
        
        //전시회 숨김코드
        label_find_account.hidden = true
        auto_login_checkbox.hidden = true
        auto_login_label.hidden = true
        
        auto_login_checkbox.boxType = .Square
        
        //키보드 숨기기
        let keyboardHideGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        self.view.addGestureRecognizer(keyboardHideGesture)
        
    }
    
    func onClickFindAccount(){
    }
    
    func hideKeyboard(){
        self.view.endEditing(true)
    }
    
}
