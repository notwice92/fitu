//
//  NetDefine.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 3..
//  Copyright © 2016년 FITU. All rights reserved.
//

import Foundation

class NetDefine {
    static let baseURL = "http://52.79.199.22/fitflow"
    static let login = baseURL + "/member/login.php"
    static let schedule = baseURL + "/schedule/list.php"
    static let fitness_grade = baseURL + "/member/get_fitness_grade.php"
    /*
     REQEUST
     member_email=회원 이메일
     member_pwd=해시 암호화 코드
     
     RESPONSE
     member_name=회원 이름
     member_email=문자열
     confirm_message=True/False
    */
}