//
//  FITMovementView.swift
//  FITU
//
//  Created by Yenos on 2016. 7. 10..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class FITMovementView: UIView,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var tableFirst: UITableView!
    @IBOutlet weak var tableSecond: UITableView!
//    @IBOutlet weak var tableSecond: UITableView!
    let arrData0 = ["ASLR", "SM", "RS", "TSPU"]
    let arrData1 = ["ILL","HS","DS","",""]

    override init(frame: CGRect) {
        
        super.init(frame:frame)
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    func setDelagte(fitMove: FITMovementView){
        
        self.tableFirst.delegate = fitMove
        self.tableFirst.dataSource = fitMove
        
        self.tableSecond.dataSource = fitMove
        self.tableSecond.delegate = fitMove
        
        self.tableFirst.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tableFirst.scrollEnabled = false
        
        self.tableSecond.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tableSecond.scrollEnabled = false
        
    }
    
    // MARK:  UITextFieldDelegate Methods
    //색션은 한개로고정.
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    //카운트도
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == tableFirst){
            return arrData0.count
        }else{
            //숫자는 같지만 나눠서 생각하도록 한다.
            return 4
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        
        if(tableView==tableFirst){
            
           let textCellIdentifier = "ANALYSIS_CELL_0"
            let  cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier, forIndexPath: indexPath) as! FITAnaylsisTableViewCell0
            
            cell.labelNum.text = String(indexPath.row+1)
            cell.labelTitle.text = arrData0[indexPath.row]
            cell.labelValue.text = "L 1 L 2"
            cell.selectionStyle = UITableViewCellSelectionStyle.None

            return cell
        }else{
            
          let  textCellIdentifier = "ANALYSIS_CELL_1"
            let cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier, forIndexPath: indexPath) as! FITAnalaysisTableViewCell1
            if(indexPath.row==3){
                cell.labelTitle.hidden = true
                
                cell.labelNum.text = "TOTAL"
                cell.labelNum.font = UIFont(name: "AppleSDGothicNeo-Medium", size: 17)
                cell.labelNum.textColor = UIColor.colorFromHex("#B1B0B0")
                
                cell.labelValue.text = "21"
                cell.labelValue.textColor = UIColor.colorFromHex("#B1B0B0")
                
            }else{
                cell.labelNum.text = String(indexPath.row+5)
                cell.labelTitle.text = arrData0[indexPath.row]
                cell.labelValue.text = "L 1 L 2"
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell
            
        }
        
    }
    
    func initRadarGraph(){
        print("call init")
//        var customView = UIView(frame: CGRectZero(top: 0,left: 0, width: 200, height: 50)
//            
//        self.view.addSubview(customView)
        
        let radarChart = Yashin(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        
//        let uivi = UIView(frame:  CGRect(x: 0, y: 0, width: 100, height: 100))
//        uivi.backgroundColor = UIColor.redColor()
//        backView.addSubview(uivi)
        

        self.addSubview(radarChart)
        
        radarChart.set(
            ["SQ", "HU", "LU", "SH", "LG", "TR", "RO"],
            [
            ([7,6,7,7,8,8,6], UIColor.grayColor().colorWithAlphaComponent(0.40)),
            ([8,7,8,7,7,9,5], UIColor.blackColor().colorWithAlphaComponent(0.70))
            ]
        )
        
        
        radarChart.translatesAutoresizingMaskIntoConstraints = false
//        graphView2.translatesAutoresizingMaskIntoConstraints = false
        
        //기본 제약사항 코드로
        Utilz.defaultConstraints(radarChart, backView:backView)
//        Utilz.defaultConstraints(graphView2, backView:viewBarBack)

    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
