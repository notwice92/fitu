//
//  RecodeTableViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 10..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class RecodeTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var classTitleLabel: UILabel!
    
    @IBOutlet weak var recodeTextField: UITextField!{
        didSet {
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.Default
            toolBar.translucent = true
            toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: "완료", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.donePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            
            toolBar.setItems([spaceButton, doneButton], animated: false)
            toolBar.userInteractionEnabled = true
            
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            self.recodeTextField.inputView = picker
            self.recodeTextField.inputAccessoryView = toolBar
        }
    }
    
    internal enum RecodeType {
        case STRENGTH, MUSCULARENDUANCE, CARDIOENDUANCE, MOVEMENT, MOVEMENTBALANCE, AMRAP, TIME, DEFAULT
    }
    
    var type:RecodeType = .DEFAULT
    
    func donePicker() {
        recodeTextField.resignFirstResponder()
    }
    
    //MARK: picker
    var message = [0,0,0,0,0]
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        switch type {
        case .STRENGTH:
            return 2
        case .MUSCULARENDUANCE:
            return 2
        case .CARDIOENDUANCE:
            return 4
        case .MOVEMENT:
            return 1
        case .MOVEMENTBALANCE:
            return 4
        case .AMRAP:
            return 4
        case .TIME:
            return 5
        default:
            return 1
        }
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch type {
        case .STRENGTH:
            if component == 0 {
                return 200
            }else {
                return 2
            }
        case .MUSCULARENDUANCE:
            if component == 0 {
                return 200
            }else {
                return 1
            }
        case .CARDIOENDUANCE:
            if component == 0 || component == 2 {
                return 200
            }else {
                return 1
            }
        case .MOVEMENT:
            return 200
        case .MOVEMENTBALANCE:
            if component == 0 || component == 2 {
                return 4
            }else {
                return 1
            }
        case .AMRAP:
            if component == 0{
                return 2
            }else if component == 2 {
                return 1
            }else {
                return 200
            }
        case .TIME:
            if component == 0{
                return 2
            }else if component == 2 {
                return 1
            }else if component == 4 {
                return 1
            }else{
                return 200
            }
        default:
            return 1
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        message[component] = row
        var txt = ""
        for i in 0...(pickerView.numberOfComponents-1){
            txt += array[i]![message[i]] + " "
        }
        self.recodeTextField.text = txt
    }
    
    var array = [0:[String](),1:[String](),2:[String](),3:[String](),4:[String]()]
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var title = "";
        switch type {
        case .STRENGTH:
            if component == 0 {
                title = "\(row)"
            }else {
                if row == 0 {
                    title = "lbs"
                }else {
                    title = "kg"
                }
            }
        case .MUSCULARENDUANCE:
            if component == 0 {
                title = "\(row)"
            }else {
                title = "reps"
            }
        case .CARDIOENDUANCE:
            if component == 0 || component == 2 {
                title = "\(row)"
            }else if component == 1{
                title = "분"
            }else {
                title = "초"
            }
        case .MOVEMENT:
            title = "\(row)"
        case .MOVEMENTBALANCE:
            if component == 0 || component == 2 {
                title = "\(row)"
            }else if component == 1{
                title = "L"
            }else {
                title = "R"
            }
        case .AMRAP:
            if component == 0{
                if row == 0 {
                    title = "RXD"
                }else {
                    title = "SCALED"
                }
            }else if component == 2 {
                title = "R"
            }else {
                title = "\(row)"
            }
        case .TIME:
            if component == 0{
                if row == 0 {
                    title = "RXD"
                }else {
                    title = "SCALED"
                }
            }else if component == 2 {
                title = "분"
            }else if component == 4 {
                title = "초"
            }else{
                title = "\(row)"
            }
        default:
            title = "default"
        }
        
        var titles = array[component]! as [String]
        if !titles.contains(title){
            titles.append(title)
        }
        array[component] = titles
        return title
    }
    
}
