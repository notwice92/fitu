//
//  ClassScheduleHeaderTableViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 3..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class ClassScheduleHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var classTitleLabel: UILabel!
    
    @IBOutlet weak var fiflCountLabel: UILabel!
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}