//
//  RankerTableViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 13..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class RankerTableViewCell: UITableViewCell {

    @IBOutlet weak var rankLabel: UILabel!
    
    @IBOutlet weak var rankerProfile: UIImageView!{
        didSet{
            rankerProfile.layer.cornerRadius = rankerProfile.frame.width / 2
        }
    }
    
    @IBOutlet weak var rankerNameLabel: UILabel!
    
    @IBOutlet weak var registerLabel: UILabel!
    
    @IBOutlet weak var recodeTypeLabel: UILabel!
    
    @IBOutlet weak var recodeValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
