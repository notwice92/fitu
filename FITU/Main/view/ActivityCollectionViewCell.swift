//
//  ActivityCollectionViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 3..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class ActivityCollectionViewCell: UICollectionViewCell {
    
    var delegate:ActivityCollectionViewCellDelegate!
    var isExpand:Bool = false
    
    @IBOutlet weak var activityTypeLabel: UILabel!
    
    @IBOutlet weak var activityTitleLabel: UILabel!
    
    @IBOutlet weak var expandButton: UIButton!
    
    @IBAction func onClickExpandButton(sender: AnyObject) {
        if delegate != nil {
            delegate.onClickExpandButton(isExpand)
            isExpand = !isExpand
        }
    }
}
protocol ActivityCollectionViewCellDelegate {
    func onClickExpandButton(isExpand:Bool)
}