//
//  ClassWarmCoolTableViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 3..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class ClassWarmCoolTableViewCell: UITableViewCell {
    
    var delegate:ClassWarmCoolTableViewCellDelegate!
    var isExpand = false
    var isCool = false

    @IBOutlet weak var activityTypeLabel: UILabel!
    
    @IBOutlet weak var activityTitleLabel: UILabel!

    @IBOutlet weak var expandButton: UIButton!
    
    @IBAction func onClickExpandButton(sender: AnyObject) {
        if delegate != nil {
            delegate.onClickExpand(isExpand, isCool: isCool)
            isExpand = !isExpand
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
protocol ClassWarmCoolTableViewCellDelegate {
    func onClickExpand(isExpand: Bool, isCool: Bool)
}
