//
//  ActivityDetailTableViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 3..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class ActivityDetailTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var methodLabel: UILabel!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBAction func onSegmentChanged(sender: AnyObject) {
        tableView.reloadData()
        if delegate != nil {
            delegate.onSegmentChanged(segmentedControl.selectedSegmentIndex)
        }
    }
    
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var viewContainerTopConstraints: NSLayoutConstraint!
    
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    var delegate:OnClickScaleCellDelegate!
    
    //MARK: tableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0 {
            return 2
        }else {
            return 3
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if segmentedControl.selectedSegmentIndex == 0 {
            return CGFloat(56)
        }else {
            if indexPath.row == 0 && segmentedControl.selectedSegmentIndex == 1{
                return CGFloat(30)
            }else {
                return CGFloat(56)
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 && segmentedControl.selectedSegmentIndex == 1 {
            return tableView.dequeueReusableCellWithIdentifier("ExerciseScaleTableViewCell", forIndexPath: indexPath) as! ExerciseScaleTableViewCell
        }else if indexPath.row == 1{
            return tableView.dequeueReusableCellWithIdentifier("ExerciseDetailTableViewCell", forIndexPath: indexPath) as! ExerciseDetailTableViewCell
        }else {
            return tableView.dequeueReusableCellWithIdentifier("ExerciseTableViewCell", forIndexPath: indexPath) as! ExerciseTableViewCell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if segmentedControl.selectedSegmentIndex == 1 {
            if indexPath.row == 0 {
                if delegate != nil {
                    delegate.onClickScaleCell()
                }
            }
        }
    }
}

protocol OnClickScaleCellDelegate{
    func onClickScaleCell()
    func onSegmentChanged(position:Int)
}
