//
//  ScaledCollectionViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 15..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class ScaledCollectionViewCell: UICollectionViewCell, VerticalSliderDelegate {
    
    @IBOutlet weak var label_info: UILabel!
    @IBOutlet weak var label_title: UILabel!
    @IBOutlet weak var slider: NapySlider!{
        didSet{
            slider.delegate = self
        }
    }
    
    @IBOutlet weak var indicateLabel: UILabel!
    
    func handleMoved(position: Double) {
        if (position == 0){
            indicateLabel.text = "낮게"
        } else if (position == 1){
            indicateLabel.text = "적당"
        } else {
            indicateLabel.text = "무겁게"
        }
    }
}
