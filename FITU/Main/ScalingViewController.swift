//
//  ScalingViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 15..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit
import Alamofire
import JSON

class ScalingViewController: UIViewController, UICollectionViewDataSource {

    @IBAction func onClickDone(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    let titles = ["MONOSTRUCTURAL", "WEIGHTLIFTING", "GYMNASIC"]
    let info = ["심폐지구력 수준 A", "근력 수준 B", "근지구력"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    func getGradeInfo(){
        Alamofire.request(.POST, NetDefine.fitness_grade, parameters: ["member_email": appDelegate.userId])
            .validate()
            .responseString(completionHandler: { response in
                switch response.result {
                case .Success:
                    print("result----------------------------------------")
                    print(response.result.value)
                 
                case .Failure(let error):
                    print(error)
                }
            })
            .responseData(completionHandler: { response in
                switch response.result {
                case .Success:
                    print("result----------------------------------------")
                    print(response.result.value)
                    do {
                        let JSON = try response.result.value?.toJSON() as? [String : AnyObject]
                        print(JSON)
                    } catch {
                        // Handle error
                    }
                case .Failure(let error):
                    print(error)
                }
            })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - collectionView
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ScaledCollectionViewCell", forIndexPath: indexPath) as! ScaledCollectionViewCell
        cell.label_title.text = titles[indexPath.row]
        cell.label_info.text = info[indexPath.row]
        return cell
    }
}
