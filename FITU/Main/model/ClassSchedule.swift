//
//  ClassSchedule.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 3..
//  Copyright © 2016년 FITU. All rights reserved.
//

import Foundation

class ClassSchedule {
    
    var class_id:String!
    var name:String!
    var date:NSDate!
    var fifl:Int!
    var programs:[Program]!
    var activitys:[Activity]!
    
    init(class_id: String, name: String, date: String, fifl: Int, programs: [Program], activitys: [Activity]){
        self.class_id = class_id
        self.name = name
        self.date = date.networkStringDate()
        self.fifl = fifl
        self.programs = programs
        self.activitys = activitys
    }
    
}