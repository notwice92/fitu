//
//  PrivateSchedule.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 3..
//  Copyright © 2016년 FITU. All rights reserved.
//

import Foundation

class PrivateSchedule {
    var title:String!
    var date:NSDate!
    var type:String!
    
    init(title: String, date: String, type: String){
        self.title = title
        self.date = date.networkStringDate()
        self.type = type
    }
}