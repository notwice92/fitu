//
//  Activity.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 14..
//  Copyright © 2016년 FITU. All rights reserved.
//

import Foundation

class Activity {
    var count:String!
    var count_scaled:String!
    var exercise_id:String!
    var name:String!
    var program_id:String!
    var unit:String!
    var utility:String!
    var utility_unit:String!
    
    init(count:String, count_scaled:String, exercise_id:String, name:String, program_id:String, unit:String, utility:String, utility_unit:String){
        self.count = count
        self.count_scaled = count_scaled
        self.exercise_id = exercise_id
        self.name = name
        self.program_id = program_id
        self.unit = unit
        self.utility = utility
        self.utility_unit = utility_unit
    }
}