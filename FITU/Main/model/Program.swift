//
//  Program.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 16..
//  Copyright © 2016년 FITU. All rights reserved.
//

import Foundation

class Program {
    var name:String!
    var program_id:String!
    var type:String!
    
    init(name: String, program_id: String, type: String){
        self.name = name
        self.program_id = program_id
        self.type = type
    }
}
