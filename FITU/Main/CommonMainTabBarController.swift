//
//  CommonMainTabBarController.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 19..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class CommonMainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var controllers = [UIViewController]()
        
        let commonMainST = UIStoryboard(name: "CommonMain", bundle: nil)
        
        let introVC = UIStoryboard(name: "Web", bundle: nil).instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        introVC.tabBarItem = UITabBarItem(title: "FITFLOW", image: UIImage(named: "ic_tab_fitflow"), tag: 1)
        controllers.append(introVC)
        
        let scheduleVC = commonMainST.instantiateViewControllerWithIdentifier("ScheduleTabViewController") as! ScheduleTabViewController
        scheduleVC.tabBarItem = UITabBarItem(title: "다이어리", image: UIImage(named: "ic_tab_diary"), tag: 2)
        controllers.append(scheduleVC)
        
        let rankingVC = commonMainST.instantiateViewControllerWithIdentifier("RankingViewController") as! RankingViewController
        rankingVC.tabBarItem = UITabBarItem(title: "랭킹", image: UIImage(named: "ic_tab_ranking"), tag: 1)
        controllers.append(rankingVC)
        
        let workoutVC = commonMainST.instantiateViewControllerWithIdentifier("WorkOutViewController") as! WorkOutViewController
        workoutVC.tabBarItem = UITabBarItem(title: "운동일지", image: UIImage(named: "ic_tab_workout"), tag: 1)
        controllers.append(workoutVC)
        
        let mainST = UIStoryboard(name: "Main", bundle: nil)
        let mypageVC = mainST.instantiateViewControllerWithIdentifier("FITMyInfoViewController") as! FITMyInfoViewController
        mypageVC.tabBarItem = UITabBarItem(title: "분석", image: UIImage(named: "ic_tab_mypage"), tag: 5)
        controllers.append(mypageVC)
        
        self.setViewControllers(controllers, animated: false)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
