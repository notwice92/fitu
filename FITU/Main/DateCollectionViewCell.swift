//
//  DateCollectionViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 22..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell {
    override var selected: Bool {
        didSet {
            if self.selected {
                dateLabel.textColor = UIColor.whiteColor()
                indicatorView.backgroundColor = UIColor.colorFromHex("#C9C9CA")
                indicatorView.layer.cornerRadius = indicatorView.frame.height/2
                
                if delegate != nil {
                    delegate.updateSelectedDate(date)
                }
            }else {
                dateLabel.textColor = UIColor.blackColor()
                indicatorView.backgroundColor = UIColor.whiteColor()
            }
        }
    }
    
    var date: NSDate! {
        didSet{
            dateLabel.text = date.dayString()
        }
    }
    
    var delegate: DateCollectionViewCellDelegate!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var indicatorView: UIView!
    
    @IBOutlet weak var indicate1: UIView! {
        didSet{
            indicate1.layer.cornerRadius = indicate1.frame.height/2
        }
    }
    
}
protocol DateCollectionViewCellDelegate {
    func updateSelectedDate(date: NSDate)
}
