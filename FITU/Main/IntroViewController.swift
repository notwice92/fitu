//
//  IntroViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 19..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate {

    let items: [(image: String, url: String)] = [
        ("main01", "The first image from space"),
        ("main02", "The second image from space"),
        ("main03", "The third image from space"),
        ("main04", "The fourth image from space")]

    let introitems: [(image: String, name: String, job: String)] = [
        ("introitem1", "박은성", "헤드코치"),
        ("introitem2", "CROSSFIT VOLCANO", "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: HorizontalScroll
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("IntroImageCollectionViewCell", forIndexPath: indexPath) as! IntroImageCollectionViewCell
        let item = items[indexPath.item]
        cell.imageview.image = UIImage(named: item.image)
        return cell
    }
    
    //MARK: TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + introitems.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("horizontalScroll", forIndexPath: indexPath)
            return cell
        }else {
            let cell = tableView.dequeueReusableCellWithIdentifier("IntroImageTableViewCell", forIndexPath: indexPath) as! IntroImageTableViewCell
            cell.imageview.image = UIImage(named: introitems[indexPath.row-1].image)
            cell.name.text = introitems[indexPath.row-1].name
            cell.job.text = introitems[indexPath.row-1].job
            return cell
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storyboard = UIStoryboard(name: "Web", bundle: nil)
        let webView = storyboard.instantiateViewControllerWithIdentifier("WebViewController")
        self.navigationController?.presentViewController(webView, animated: true, completion: nil)
    }
    
}
