//
//  ScheduleTabViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 22..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit
import Alamofire
import JSON

class ScheduleTabViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, ActivityCollectionViewCellDelegate, ClassWarmCoolTableViewCellDelegate, OnClickScaleCellDelegate, DateCollectionViewCellDelegate {
    
    @IBAction func onClickRecode(sender: AnyObject) {
        var controllers = self.tabBarController?.viewControllers
        let recodeVC = (self.storyboard?.instantiateViewControllerWithIdentifier("RecodeViewController"))!
        recodeVC.tabBarItem = self.tabBarItem
        controllers![1] = recodeVC
        self.tabBarController?.setViewControllers(controllers, animated: false)
    }
    
    @IBOutlet weak var yearLabel: UILabel!
    
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBAction func onClickAfter(sender: AnyObject) {
        currentWeek = NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 + Double((7) * 60 * 60 * 24))
    }
    
    @IBAction func onClickBefore(sender: AnyObject) {
        currentWeek = NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 - Double((7) * 60 * 60 * 24))
    }
    
    @IBOutlet weak var collectionView: UICollectionView! // calendar
    
    var daysData: [String] = ["S", "M", "T", "W", "T", "F", "S"]
    var currentWeek: NSDate!{
        didSet{
            print(currentWeek)
            
            yearLabel.text = currentWeek.yearString()
            monthLabel.text = currentWeek.monthString()
            
            collectionView.reloadData()
            
            loadingSchedule()
        }
    }
    var selectedDate = NSDate(){
        didSet{
            activityTableView.reloadData()
        }
    }
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    func loadingSchedule(){
        Alamofire.request(.POST, NetDefine.schedule, parameters: ["member_email": appDelegate.userId, "start_date":self.currentWeek.networkDateString(), "end_date":NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 + Double((7) * 60 * 60 * 24)).networkDateString()])
            .validate()
            .responseData(completionHandler: { response in
                switch response.result {
                case .Success:
                    self.privateScheduleArray.removeAll()
                    self.classScheduleArray.removeAll()
                    do {
                        let JSON = try response.result.value?.toJSON() as? [String : AnyObject]
                        print(JSON)
                        
                        let scheduleList = JSON!["schedule_lists"] as! [[String : AnyObject]]
                        
                        for sch in scheduleList {
                            print(sch)
                            print("=====================")
                            
                            let privates = sch["private"] as! [[String : AnyObject]]
                            for p in privates {
                                print("*********************************")
                                print(p)
                                print("*********************************")
                                
                                let ss = p["schedule"] as! [AnyObject]
                                for s in ss {
                                    let date = s["date"] as! String
                                    let name = s["name"] as! String
                                    let type = s["type"] as! String
                                    
                                    self.privateScheduleArray.append(PrivateSchedule(title: name, date: date, type: type))
                                }
                            }
                            
                            let classes = sch["class"] as! [[String : AnyObject]]
                            for c in classes {
                                let class_id = c["class_id"] as! String
                                let name = c["name"] as! String
                                let date = c["date"] as! String
                                let fifl = c["fifl"] as! Int
                                var programs = [Program]()
                                for program in c["program"] as! [AnyObject] {
                                    let name = program["name"] as! String
                                    let program_id = program["program_id"] as! String
                                    let type = program["type"] as! String
                                    
                                    programs.append(Program(name: name, program_id: program_id, type: type))
                                }
                                var activitys = [Activity]()
                                for activity in c["exercise"] as! [AnyObject] {
                                    let count = activity["count"] as! String
                                    let count_scaled = activity["count_scaled"] as! String
                                    let exercise_id = activity["exercise_id"] as! String
                                    let name = activity["name"] as! String
                                    let program_id = activity["program_id"] as! String
                                    var unit = "null"
                                    if let u = activity["unit"] as? String {
                                        unit = u
                                    }
                                    var utility = "null"
                                    if let util = activity["utility"] as? String {
                                        utility = util
                                    }
                                    var utility_unit = "null"
                                    if let util_unit = activity["utility_unit"] as? String {
                                        utility_unit = util_unit
                                    }
                                    
                                    activitys.append(Activity(count: count, count_scaled: count_scaled, exercise_id: exercise_id, name: name, program_id: program_id, unit: unit, utility: utility, utility_unit: utility_unit))
                                }
                                
                                self.classScheduleArray.append(ClassSchedule(class_id: class_id, name: name, date: date, fifl: fifl, programs: programs, activitys: activitys))
                                
                                self.collectionView.reloadData()
                                
                            }
                        }
                    } catch {
                        // Handle error
                    }
                case .Failure(let error):
                    print(error)
                }
            })
    }
    
    //MARK: Schedule Arrays
    var privateScheduleArray = [PrivateSchedule]() //key(date) - value(array)
    var classScheduleArray = [ClassSchedule]() //key(date) - value(array)
    
    enum ActivityCellType {
        case WARM, WARMLIST, ACTIVITY, ACTIVITYLIST, COOL, COOLLIST
    }
    
    @IBOutlet weak var activityTableView: UITableView!
    var activityArray:[ActivityCellType] = [ActivityCellType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let today = NSDate()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.Weekday, fromDate: today)
        let weekDay = myComponents.weekday
        
        currentWeek = NSDate(timeIntervalSince1970: today.timeIntervalSince1970 - Double((weekDay-1) * 60 * 60 * 24))
        
        //swipe
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.onClickAfter(_:)))
        leftSwipe.direction = .Left
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.onClickBefore(_:)))
        rightSwipe.direction = .Right
        
        self.collectionView.addGestureRecognizer(leftSwipe)
        self.collectionView.addGestureRecognizer(rightSwipe)
        
        activityArray.append(.WARM)
        activityArray.append(.ACTIVITY)
        activityArray.append(.COOL)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: CollectionView for calendar
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if collectionView == self.collectionView{ // calendar
            return 2
        }else {
            return 1
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView{ // calendar
            return 7
        }else {
            var count = 0
            for a in self.classScheduleArray {
                if selectedDate.networkDateString() == a.date.networkDateString() {
                    count = a.programs.count - 2
                }
            }
            return count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView{ // calendar
            if indexPath.section == 0 {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DayCollectionViewCell", forIndexPath: indexPath) as! DayCollectionViewCell
                cell.dayLabel.text = daysData[indexPath.row]
                return cell
            }else {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DateCollectionViewCell", forIndexPath: indexPath) as! DateCollectionViewCell
                cell.date = NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 + Double((indexPath.row) * 60 * 60 * 24))
                cell.delegate = self
                cell.indicate1.hidden = true
                for p in self.privateScheduleArray {
                    if cell.date.networkDateString() == p.date.networkDateString() {
                        cell.indicate1.hidden = false
                    }
                }
                
                for a in self.classScheduleArray {
                    if cell.date.networkDateString() == a.date.networkDateString() {
                        cell.indicate1.hidden = false
                    }
                }
                return cell
            }
        }else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ActivityCollectionViewCell", forIndexPath: indexPath) as! ActivityCollectionViewCell
            cell.delegate = self
            
            let p = progs[indexPath.row]
            cell.activityTitleLabel.text = p.name
            
            var hasChild = false
            for a in acts {
                if a.program_id == p.program_id {
                    hasChild = true
                }
            }
            
            if hasChild {
                cell.activityTypeLabel.text = "MULTI"
                cell.expandButton.hidden = false
            }else {
                cell.activityTypeLabel.text = "STRENGTH"
                cell.expandButton.hidden = true
            }
            
            return cell
        }
    }
    
    //MARK: ActivityCollectionViewCellDelegate
    func onClickExpandButton(isExpand: Bool) {
        if isExpand {
            activityArray.removeAtIndex(activityArray.indexOf(.ACTIVITYLIST)!)
        }else {
            activityArray.insert(.ACTIVITYLIST, atIndex: 1 + activityArray.indexOf(.ACTIVITY)!)
        }
        activityTableView.reloadData()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if collectionView == self.collectionView{ // calendar
            let screenRect = UIScreen.mainScreen().bounds
            let height:CGFloat
            let width = (screenRect.width - 70) / 7
            
            height = indexPath.section == 0 ? CGFloat(15) : CGFloat(41)
            
            return CGSize(width: width, height: height)
        }else {
            return collectionView.bounds.size
        }
    }
    
    //MARK: DateCollectionViewCellDelegate
    func updateSelectedDate(date: NSDate) {
        self.selectedDate = date
    }
    
    //MARK: TableView for exercise Schedule
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: //회원일정
            var count = 0
            for p in self.privateScheduleArray{
                if p.date.networkDateString() == selectedDate.networkDateString() {
                    count += 1
                }
            }
            if count > 1 {
                return 1
            }
            return count
        case 1:
            for cls in classScheduleArray {
                if cls.date.networkDateString() == selectedDate.networkDateString(){
                    return 1
                }
            }
            return 0
        default: //클래스 일정
            for cls in classScheduleArray {
                if cls.date.networkDateString() == selectedDate.networkDateString(){
                    return activityArray.count
                }
            }
            return 0
        }
    }
    
    var scaled = false
    func onSegmentChanged(position: Int) {
        scaled = (position == 1)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: //회원일정
            return CGFloat(30)
        case 1:
            return CGFloat(60)
        default: //클래스 일정
            switch self.activityArray[indexPath.row] {
            case .ACTIVITY:
                return CGFloat(100)
            case .ACTIVITYLIST:
                return scaled ? CGFloat(154 + (54 * 2) + 30) : CGFloat(154 + (54 * 2))
            case .COOLLIST:
                return CGFloat(54 * 2)
            case .WARMLIST:
                return CGFloat(54 * 2)
            default:
                return CGFloat(90)
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0: //회원일정
            let cell = tableView.dequeueReusableCellWithIdentifier("LabelUITableViewCell", forIndexPath: indexPath) as! LabelUITableViewCell
            cell.backgroundColor = UIColor.colorFromHex("#f5f5f5")
            
            for p in self.privateScheduleArray{
                if p.date.networkDateString() == selectedDate.networkDateString() {
                    cell.scheduleTitleLabel.text = p.title
                }
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("ClassScheduleHeaderTableViewCell", forIndexPath: indexPath) as! ClassScheduleHeaderTableViewCell
            cell.timeLabel.text = "09:00-10:00"
            for cls in classScheduleArray {
                if cls.date.networkDateString() == selectedDate.networkDateString(){
                    cell.classTitleLabel.text = cls.name
                    cell.fiflCountLabel.text = "\(cls.fifl)"
                }
            }
            return cell
        default: //클래스 일정
            var currentClass: ClassSchedule!
            for cls in classScheduleArray {
                if cls.date.networkDateString() == selectedDate.networkDateString(){
                    currentClass = cls
                }
            }
            
            switch self.activityArray[indexPath.row] {
            case .WARM:
                let cell = tableView.dequeueReusableCellWithIdentifier("ClassWarmCoolTableViewCell", forIndexPath: indexPath) as! ClassWarmCoolTableViewCell
                cell.delegate = self
                cell.isCool = false
                cell.activityTypeLabel.text = "WARM UP"
                if currentClass != nil {
                    for program in currentClass.programs {
                        if program.type == "1" {
                            for act in currentClass.activitys {
                                if act.program_id == program.program_id {
                                    cell.activityTitleLabel.text = act.name
                                }
                                if act.unit == "null" {
                                    cell.expandButton.hidden = true
                                }else {
                                    cell.expandButton.hidden = false
                                }
                            }
                        }
                    }
                }
                return cell
            case .WARMLIST:
                let cell = tableView.dequeueReusableCellWithIdentifier("ActivityDetailTableViewCell", forIndexPath: indexPath) as! ActivityDetailTableViewCell
                cell.viewContainer.hidden = true
                cell.viewContainerTopConstraints.constant = 0
                cell.updateConstraints()
                return cell
            case .COOL:
                let cell = tableView.dequeueReusableCellWithIdentifier("ClassWarmCoolTableViewCell", forIndexPath: indexPath) as! ClassWarmCoolTableViewCell
                cell.delegate = self
                cell.isCool = true
                cell.activityTypeLabel.text = "COOL DOWN"
                if currentClass != nil {
                    for program in currentClass.programs {
                        if program.type == "3" {
                            for act in currentClass.activitys {
                                if act.program_id == program.program_id {
                                    cell.activityTitleLabel.text = act.name
                                }
                                if act.unit == "null" {
                                    cell.expandButton.hidden = true
                                }else {
                                    cell.expandButton.hidden = false
                                }
                            }
                        }
                    }
                }
                return cell
            case .COOLLIST:
                let cell = tableView.dequeueReusableCellWithIdentifier("ActivityDetailTableViewCell", forIndexPath: indexPath) as! ActivityDetailTableViewCell
                cell.viewContainer.hidden = true
                cell.viewContainerTopConstraints.constant = 0
                cell.updateConstraints()
                return cell
            case .ACTIVITY:
                let cell = tableView.dequeueReusableCellWithIdentifier("ActivitiesTableViewCell", forIndexPath: indexPath) as! ActivitiesTableViewCell
                
                progs.removeAll()
                acts.removeAll()
                
                if currentClass != nil {
                    for program in currentClass.programs {
                        if program.type == "2" {
                            progs.append(program)
                            for act in currentClass.activitys {
                                acts.append(act)
                            }
                        }
                    }
                }
                
                return cell
            case .ACTIVITYLIST:
                let cell = tableView.dequeueReusableCellWithIdentifier("ActivityDetailTableViewCell", forIndexPath: indexPath) as! ActivityDetailTableViewCell
                cell.viewContainer.hidden = false
                cell.viewContainerTopConstraints.constant = 146
                cell.updateConstraints()
                cell.delegate = self
                return cell
            }
        }
    }
    
    var acts = [Activity]()
    var progs = [Program]()
    
    func onClickScaleCell() {
        let viewController = UIStoryboard(name: "CommonMain", bundle: nil).instantiateViewControllerWithIdentifier("ScalingViewController") as! ScalingViewController
        viewController.modalPresentationStyle = .Popover
        viewController.preferredContentSize = CGSizeMake(343, 512)
        
        presentViewController(viewController, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let change = UITableViewRowAction(style: .Normal, title: "변경") { action, index in
            print("share button tapped")
        }
        change.backgroundColor = UIColor.blackColor()
        return [change]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        if indexPath.section == 1 && indexPath.row != 0 {
            return true
        }else {
            return false
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // you need to implement this method too or you can't swipe to display the actions
    }
    
    //MARK: ClassWarmCoolTableViewCellDelegate
    func onClickExpand(isExpand: Bool, isCool: Bool) {
        if isExpand {
            if isCool {
                activityArray.removeAtIndex(activityArray.indexOf(.COOLLIST)!)
            }else {
                activityArray.removeAtIndex(activityArray.indexOf(.WARMLIST)!)
            }
        }else {
            if isCool{
                activityArray.insert(.COOLLIST, atIndex: 1 + activityArray.indexOf(.COOL)!)
            }else{
                activityArray.insert(.WARMLIST, atIndex: 1 + activityArray.indexOf(.WARM)!)
            }
        }
        activityTableView.reloadData()
    }
    
}
