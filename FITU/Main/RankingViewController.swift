//
//  RankingViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 12..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class RankingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var sexSelecter: UISegmentedControl!
    
    @IBAction func onSexSelectorValueChange(sender: AnyObject) {
        
    }
    
    var rankerArray = [Ranker]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sexSelecter.layer.cornerRadius = 0.0;
        sexSelecter.layer.borderWidth = 1.0;
        sexSelecter.layer.masksToBounds = true;
        sexSelecter.tintColor = UIColor.blackColor()
        sexSelecter.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.blackColor()], forState: UIControlState.Normal)
        sexSelecter.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.whiteColor()], forState: UIControlState.Focused)
        // Do any additional setup after loading the view.
    }
    
    // MARK: TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        default: //ranker
            return rankerArray.count
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: //운동목록
            return CGFloat(56)
        case 1: //운동목록
            return CGFloat(30)
        default: //클래스 일정
            return CGFloat(56)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0: //운동 리스트
            let cell = tableView.dequeueReusableCellWithIdentifier("RankerTableViewCell", forIndexPath: indexPath) as! RankerTableViewCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("RankingHaederTableViewCell", forIndexPath: indexPath)
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier("RankerTableViewCell", forIndexPath: indexPath) as! RankerTableViewCell
            return cell
        }
    }
}
