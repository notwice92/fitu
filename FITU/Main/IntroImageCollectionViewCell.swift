//
//  IntroImageCollectionViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 19..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class IntroImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview: UIImageView!
}
