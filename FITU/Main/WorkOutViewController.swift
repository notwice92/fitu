//
//  WorkOutViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 12..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class WorkOutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var mButton: UIButton!
    
    @IBOutlet weak var gButton: UIButton!
    
    @IBOutlet weak var wButton: UIButton!
    
    @IBAction func onClickmButton(sender: AnyObject) {
        mButton.selected = !mButton.selected
        if mButton.selected {
            setSelectedButtonDesign(mButton)
        }else {
            setNormalButtonDesign(mButton)
        }
    }
    
    @IBAction func onClickgButton(sender: AnyObject) {
        gButton.selected = !gButton.selected
        if gButton.selected {
            setSelectedButtonDesign(gButton)
        }else {
            setNormalButtonDesign(gButton)
        }
    }
    
    @IBAction func onClickwButton(sender: AnyObject) {
        wButton.selected = !wButton.selected
        if wButton.selected {
            setSelectedButtonDesign(wButton)
        }else {
            setNormalButtonDesign(wButton)
        }
    }
    
    func setSelectedButtonDesign(button: UIButton){
        button.setTitleColor(UIColor.blackColor(), forState: .Selected)
        button.layer.backgroundColor = UIColor.whiteColor().CGColor
    }
    
    func setNormalButtonDesign(button: UIButton){
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.layer.backgroundColor = UIColor.blackColor().CGColor
    }
    
    var activityArray = [Activity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mButton.layer.borderWidth = 1
        mButton.layer.borderColor = UIColor.blackColor().CGColor
        gButton.layer.borderWidth = 1
        gButton.layer.borderColor = UIColor.blackColor().CGColor
        wButton.layer.borderWidth = 1
        wButton.layer.borderColor = UIColor.blackColor().CGColor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ActivityTitleTableViewCell", forIndexPath: indexPath) as! ActivityTitleTableViewCell
        return cell
    }
    
    // MARK: - SearchBar
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.view.resignFirstResponder()
    }
}
