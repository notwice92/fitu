//
//  RecodeViewController.swift
//  FITU
//
//  Created by 조경래 on 2016. 7. 10..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit
import Alamofire
import JSON

class RecodeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    @IBAction func onClickDiary(sender: AnyObject) {
        var controllers = self.tabBarController?.viewControllers
        let scheduleVC = (self.storyboard?.instantiateViewControllerWithIdentifier("ScheduleTabViewController"))!
        scheduleVC.tabBarItem = self.tabBarItem
        controllers![1] = scheduleVC
        self.tabBarController?.setViewControllers(controllers, animated: false)
    }
    
    @IBOutlet weak var yearLabel: UILabel!
    
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBAction func onClickAfter(sender: AnyObject) {
        currentWeek = NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 + Double((7) * 60 * 60 * 24))
    }
    
    @IBAction func onClickBefore(sender: AnyObject) {
        currentWeek = NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 - Double((7) * 60 * 60 * 24))
    }
    
    @IBOutlet weak var collectionView: UICollectionView! // calendar
    
    var daysData: [String] = ["S", "M", "T", "W", "T", "F", "S"]
    var currentWeek: NSDate!{
        didSet{
            print(currentWeek)
            
            yearLabel.text = currentWeek.yearString()
            monthLabel.text = currentWeek.monthString()
            
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let today = NSDate()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.Weekday, fromDate: today)
        let weekDay = myComponents.weekday
        
        currentWeek = NSDate(timeIntervalSince1970: today.timeIntervalSince1970 - Double((weekDay-1) * 60 * 60 * 24))
        
        //swipe
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.onClickAfter(_:)))
        leftSwipe.direction = .Left
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.onClickBefore(_:)))
        rightSwipe.direction = .Right
        
        self.collectionView.addGestureRecognizer(leftSwipe)
        self.collectionView.addGestureRecognizer(rightSwipe)
        
    }
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    //MARK: Schedule Arrays
    var privateScheduleArray = [PrivateSchedule]() //key(date) - value(array)
    var classScheduleArray = [ClassSchedule]() //key(date) - value(array)
    
    func loadingSchedule(){
        Alamofire.request(.POST, NetDefine.schedule, parameters: ["member_email": appDelegate.userId, "start_date":self.currentWeek.networkDateString(), "end_date":NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 + Double((7) * 60 * 60 * 24)).networkDateString()])
            .validate()
            .responseData(completionHandler: { response in
                switch response.result {
                case .Success:
                    self.privateScheduleArray.removeAll()
                    self.classScheduleArray.removeAll()
                    do {
                        let JSON = try response.result.value?.toJSON() as? [String : AnyObject]
                        print(JSON)
                        
                        let scheduleList = JSON!["schedule_lists"] as! [[String : AnyObject]]
                        
                        for sch in scheduleList {
                            print(sch)
                            print("=====================")
                            
                            let privates = sch["private"] as! [[String : AnyObject]]
                            for p in privates {
                                print("*********************************")
                                print(p)
                                print("*********************************")
                                
                                let ss = p["schedule"] as! [AnyObject]
                                for s in ss {
                                    let date = s["date"] as! String
                                    let name = s["name"] as! String
                                    let type = s["type"] as! String
                                    
                                    self.privateScheduleArray.append(PrivateSchedule(title: name, date: date, type: type))
                                }
                            }
                            
                            let classes = sch["class"] as! [[String : AnyObject]]
                            for c in classes {
                                let class_id = c["class_id"] as! String
                                let name = c["name"] as! String
                                let date = c["date"] as! String
                                let fifl = c["fifl"] as! Int
                                var programs = [Program]()
                                for program in c["program"] as! [AnyObject] {
                                    let name = program["name"] as! String
                                    let program_id = program["program_id"] as! String
                                    let type = program["type"] as! String
                                    
                                    programs.append(Program(name: name, program_id: program_id, type: type))
                                }
                                var activitys = [Activity]()
                                for activity in c["exercise"] as! [AnyObject] {
                                    let count = activity["count"] as! String
                                    let count_scaled = activity["count_scaled"] as! String
                                    let exercise_id = activity["exercise_id"] as! String
                                    let name = activity["name"] as! String
                                    let program_id = activity["program_id"] as! String
                                    var unit = "null"
                                    if let u = activity["unit"] as? String {
                                        unit = u
                                    }
                                    var utility = "null"
                                    if let util = activity["utility"] as? String {
                                        utility = util
                                    }
                                    var utility_unit = "null"
                                    if let util_unit = activity["utility_unit"] as? String {
                                        utility_unit = util_unit
                                    }
                                    
                                    activitys.append(Activity(count: count, count_scaled: count_scaled, exercise_id: exercise_id, name: name, program_id: program_id, unit: unit, utility: utility, utility_unit: utility_unit))
                                }
                                
                                self.classScheduleArray.append(ClassSchedule(class_id: class_id, name: name, date: date, fifl: fifl, programs: programs, activitys: activitys))
                                
                                self.collectionView.reloadData()
                                
                            }
                        }
                    } catch {
                        // Handle error
                    }
                case .Failure(let error):
                    print(error)
                }
            })
    }

    
    //MARK: CollectionView for calendar
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DayCollectionViewCell", forIndexPath: indexPath) as! DayCollectionViewCell
            cell.dayLabel.text = daysData[indexPath.row]
            return cell
        }else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DateCollectionViewCell", forIndexPath: indexPath) as! DateCollectionViewCell
            cell.date = NSDate(timeIntervalSince1970: currentWeek.timeIntervalSince1970 + Double((indexPath.row) * 60 * 60 * 24))
            cell.indicate1.hidden = true
            for p in self.privateScheduleArray {
                if cell.date.networkDateString() == p.date.networkDateString() {
                    cell.indicate1.hidden = false
                }
            }
            
            for a in self.classScheduleArray {
                if cell.date.networkDateString() == a.date.networkDateString() {
                    cell.indicate1.hidden = false
                }
            }
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenRect = UIScreen.mainScreen().bounds
        let height:CGFloat
        let width = (screenRect.width - 70) / 7
        
        height = indexPath.section == 0 ? CGFloat(15) : CGFloat(41)
        
        return CGSize(width: width, height: height)
    }
    
    //MARK: Tableview
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    let types = [RecodeTableViewCell.RecodeType.STRENGTH, RecodeTableViewCell.RecodeType.MUSCULARENDUANCE, RecodeTableViewCell.RecodeType.CARDIOENDUANCE, RecodeTableViewCell.RecodeType.MOVEMENT, RecodeTableViewCell.RecodeType.MOVEMENTBALANCE, RecodeTableViewCell.RecodeType.AMRAP, RecodeTableViewCell.RecodeType.TIME]
    let type_titles = ["STRENGTH", "MUSCULARENDUANCE", "CARDIOENDUANCE", "MOVEMENT", "MOVEMENTBALANCE", "AMRAP", "TIME"]
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RecodeTableViewCell", forIndexPath: indexPath) as! RecodeTableViewCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.colorFromHex("#DBDBDB").CGColor
        cell.type = types[indexPath.row]
        cell.classTitleLabel.text = type_titles[indexPath.row]
        return cell
    }
}
