//
//  IntroImageTableViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 19..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class IntroImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var job: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
