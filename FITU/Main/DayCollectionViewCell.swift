//
//  DayCollectionViewCell.swift
//  FITU
//
//  Created by 조경래 on 2016. 6. 22..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class DayCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dayLabel: UILabel!
}
