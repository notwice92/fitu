//
//  FITPickerViewController.swift
//  FITU
//
//  Created by Yenos on 2016. 7. 10..
//  Copyright © 2016년 FITU. All rights reserved.
//

import UIKit

class FITPickerViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var pickerViews: UIPickerView!
    var isDate: Bool = false
    var types: String = ""
    
    var arrDateYear = [String]()
    var arrDateMonth = [String]()
    var arrDateDay = [String]()
    var arrPickerData = [String]()
    
    var actionSheetVC: UIAlertController!
    
    var resultKg: String!
    var resultYear: String!
    var resultMonth: String!
    var resultDay: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        print("is viewdidload")
        // Do any additional setup after loading the view.
        pickerViews.dataSource = self
        pickerViews.delegate = self
        print("is date \(isDate)")
        if(isDate){
            print("is Date!!")
            preapreDatePicker()
            resultKg = "0";
        }else{
        }
        resultKg = "50kg"

        


    }
    func preapreDatePicker(){
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: date)
        
        let year =  components.year
        let month = components.month
        let day = components.day
        
        var curYearIndex:Int!
        var curMonthsIndex:Int!
        var curDaysIndex:Int!
        
        var cntIndexing:Int!
        cntIndexing = 0;
        for i in 1800...2018 {
            cntIndexing = cntIndexing + 1
            arrDateYear.append(String(i))
            if(String(year)==String(i)){
                curYearIndex = cntIndexing
                resultYear = String(year)
            }
            
        }
        for i in 1...12 {
            arrDateMonth.append(String(i))
            if(String(month)==String(i)){
                curMonthsIndex = i
                resultMonth = String(month)
            }
        }
        for i in 1...30 {
            arrDateDay.append(String(i))
            if(String(day)==String(i)){
                curDaysIndex = i
                resultDay = String(day)
            }
        }
        print(curYearIndex)

        
        
        self.pickerViews.selectRow(curYearIndex-1, inComponent: 0, animated: true)
        self.pickerViews.selectRow(curMonthsIndex-1, inComponent: 1, animated: true)
        self.pickerViews.selectRow(curDaysIndex-1, inComponent: 2, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if(isDate){
            return 3
        }else{
            return 1
        }
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(isDate){
            if(component==0){
                return arrDateYear.count
            }else if(component==1){
                return arrDateMonth.count
            }else {
                return arrDateDay.count
            }
            
        }else{
            return arrPickerData.count
        }
        return 0
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(isDate==false){
            resultKg = arrPickerData[row]
        }
        if(isDate){
            if(component==0){
                resultYear = arrDateYear[row]
            }else if(component==1){
                resultMonth = arrDateMonth[row]
            }else{
                resultDay = arrDateDay[row]
            }
            
        }


//        print("selected ==> ",arrPickerData[row]);
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        if(isDate==false){
            pickerLabel.text = arrPickerData[row]
        }
        if(isDate){
            if(component==0){
                pickerLabel.text = arrDateYear[row]
            }else if(component==1){
                pickerLabel.text = arrDateMonth[row]
            }else{
                pickerLabel.text = arrDateDay[row]
            }
            
        }
        
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 20
        ) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 25.0
    }
    func setActionSheet(actionSheetVCC:UIAlertController){
        
        actionSheetVC = actionSheetVCC
    }

    @IBAction func pressCancel(sender: AnyObject) {
        actionSheetVC.dismissViewControllerAnimated(true, completion: {})
    }
    @IBAction func pressOk(sender: AnyObject) {
        if(isDate){
            print("result kg = \(resultYear) = \(resultMonth) = \(resultDay) ")
            var dataDict = Dictionary<String, String>()
            dataDict["type"] = types
            dataDict["resultYear"] = resultYear
            dataDict["resultMonth"] = resultMonth
            dataDict["resultDay"] = resultMonth
            NSNotificationCenter.defaultCenter().postNotificationName("NOTI_PICKER_END", object: nil,userInfo:dataDict )
            

        }else{
            print("result kg = \(resultKg)")
            
//            if(type=="BORN"){
//                
//            }else if(type=="WEIGH"){
//                
//            }else if(type=="FAT"){
//
//            }
            
            var dataDict = Dictionary<String, String>()
            dataDict["type"] = types
            dataDict["resultKG"] = resultKg
            NSNotificationCenter.defaultCenter().postNotificationName("NOTI_PICKER_END", object:nil,userInfo: dataDict)
            

        }
        
        actionSheetVC.dismissViewControllerAnimated(true, completion: {})
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
