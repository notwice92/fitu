//
//  FITMyInfoViewController.swift
//  FITU
//
//  Created by Yenos on 2016. 5. 15..
//  Copyright © 2016년 FITU. All rights reserved.
//
// 분석 메인뷰이다.
import UIKit

//class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
class FITMyInfoViewController: UIViewController {


    @IBOutlet var viewInbody: FITInBodyView!
    @IBOutlet var viewMovement: FITMovementView!
    @IBOutlet var viewStrength: FITStrengthView!
    @IBOutlet weak var viewDetailSub: UIView!
//    var actionSheetVC:UIAlertController!
    
//    @IBOutlet  var segMypage: UISegmentedControl!
    
    @IBOutlet weak var segMypagee: UISegmentedControl!
    
    @IBAction func pressHistoFilter(sender: AnyObject) {
        
//        self.presentViewController(actionSheetVC, animated: true, completion:{})

    }
    
    @IBAction func pressSeg(sender: AnyObject) {
        switch segMypagee.selectedSegmentIndex {
        case 0:
            print("seg 0");
            
            drawingInbodyView()
          
            
            break;
        case 1:
            print("seg 1");
            
            drawingMovementView()
            
            break;
        case 2:
            print("seg 2");

            drawingStrengthView()
            break;
            
        default: break
            
        }

    }
    
    func drawingStrengthView(){
        
        viewInbody.removeFromSuperview()
        viewMovement.removeFromSuperview()
        viewStrength.removeFromSuperview()
//        viewStrength.setDelagte(viewMovement)
        
        viewDetailSub.addSubview(viewStrength)
        viewStrength.drawLine()
        viewStrength.initWave()
//        viewStrength.drawLine(startpoint: CGPoint(x:10,y:100), endpint: CGPoint(x: 10,y: 10),linecolor:       UIColor.redColor().CGColor,linewidth:11.0)

        
        Animations.showAnimations(viewStrength)
        
        viewStrength.translatesAutoresizingMaskIntoConstraints = false
//        viewMovement.initRadarGraph()
        //기본 제약사항 코드로
        Utilz.defaultConstraints(viewStrength, backView:viewDetailSub)
    }
    func drawingInbodyView(){
        viewInbody.removeFromSuperview()
        viewMovement.removeFromSuperview()
        viewStrength.removeFromSuperview()
        
        viewDetailSub.addSubview(viewInbody)
        viewInbody.translatesAutoresizingMaskIntoConstraints = false
        Animations.showAnimations(viewInbody)
        //기본 제약사항 코드로
        Utilz.defaultConstraints(viewInbody, backView:viewDetailSub)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10), dispatch_get_main_queue(), { () -> Void in
            self.viewInbody.initRound()
            self.viewInbody.initBar()
        })
    }
    func drawingMovementView(){
        
        viewInbody.removeFromSuperview()
        viewMovement.removeFromSuperview()
        viewStrength.removeFromSuperview()
        
        //#### delegate를 먼저 잡아줘야 에러 없다.
        viewMovement.setDelagte(viewMovement)
        
        viewDetailSub.addSubview(viewMovement)
        
        Animations.showAnimations(viewMovement)
        
        viewMovement.translatesAutoresizingMaskIntoConstraints = false
        viewMovement.initRadarGraph()
        //기본 제약사항 코드로
        Utilz.defaultConstraints(viewMovement, backView:viewDetailSub)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if segMypagee != nil {
            
            print("seg heere")
            
            segMypagee.layer.cornerRadius = 0.0;
            segMypagee.layer.borderWidth = 1.0;
            segMypagee.layer.masksToBounds = true;
            segMypagee.tintColor = UIColor.blackColor()
            segMypagee.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.blackColor()], forState: UIControlState.Normal)
            
            segMypagee.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.whiteColor()], forState: UIControlState.Focused)
            drawingInbodyView()
//            segMypagee.selectedSegmentIndex = 0
            

        } else {
            print("seg NOoo")
        }
        
        
        
        
        
//
//        let graphView = GraphView(frame: CGRectMake(0,0,SCREEN_WIDTH*0.96 ,viewDetailSub.frame.height+5))
//        
//        let data = [4.0, 8.0, 15.0, 16.0, 23.0, 42.0]
//        let labels = ["one", "two", "three", "four", "five", "six"]
//        graphView.backgroundFillColor = UIColor.clearColor()
//        
//        graphView.rangeMax = 50
//        
//        graphView.lineWidth = 1
//        graphView.lineColor = UIColor.colorFromHex("#777777")
//        graphView.lineStyle = GraphViewLineStyle.Smooth
//        
//        graphView.shouldFill = true
//        graphView.fillType = GraphViewFillType.Gradient
//        graphView.fillColor = UIColor.colorFromHex("#555555")
//        graphView.fillGradientType = GraphViewGradientType.Linear
//        graphView.fillGradientStartColor = UIColor.colorFromHex("#555555")
//        graphView.fillGradientEndColor = UIColor.colorFromHex("#444444")
//        
//        graphView.dataPointSpacing = 80
//        graphView.dataPointSize = 2
//        graphView.dataPointFillColor = UIColor.whiteColor()
//        
//        graphView.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
//        graphView.referenceLineColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        graphView.referenceLineLabelColor = UIColor.whiteColor()
//        graphView.dataPointLabelColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
//        
//        graphView.setData(data, withLabels: labels)
//   
//        
//        
//        viewDetailSub.addSubview(graphView)
        
        


        
        
//        //액션쉬트 예제
//         actionSheetVC = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
//    
//        let rect = CGRectMake(-20, 0, self.view.bounds.size.width+10 , 300.0)
//        let actionSheetBackView = UIView(frame: rect)
//        
//        viewFilter.setActionSheet(actionSheetVC)
//        viewFilter.pickerFilter.delegate  = viewFilter;
//        viewFilter.pickerFilter.dataSource = viewFilter;
//        actionSheetBackView.addSubview(viewFilter);
//        
//        actionSheetVC.view.addSubview(actionSheetBackView)
        
        
//        viewHisto.translatesAutoresizingMaskIntoConstraints = false
//        viewDetailSub.translatesAutoresizingMaskIntoConstraints = false
//        viewDetailSub.addSubview(viewHisto)
        
//        NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(3), target: self, selector: self.viewInbody.initRound(), userInfo: nil, repeats: false)




    
//        let histoTrailling = NSLayoutConstraint(item: viewInbody, attribute: NSLayoutAttribute.TrailingMargin, relatedBy: NSLayoutRelation.Equal, toItem: viewDetailSub, attribute: NSLayoutAttribute.TrailingMargin, multiplier: 1, constant: 0)
//        // top constraint
//        let histoTop = NSLayoutConstraint(item: viewInbody, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem:viewDetailSub, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
//        // bottom constraint
//        let histoBottom = NSLayoutConstraint(item: bottomLayoutGuide, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem:viewInbody, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0)
//        // leading margin constraint
//        let histoLeading = NSLayoutConstraint(item: viewInbody, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem:viewDetailSub, attribute: NSLayoutAttribute.LeadingMargin, multiplier: 1, constant: 0)
//        
//        NSLayoutConstraint.activateConstraints([histoTrailling, histoTop, histoBottom, histoLeading])

//        Utilz.defaultConstraints(viewFilter, backView: actionSheetBackView)


        
    }
    @IBAction func pressRecord(sender: AnyObject) {
//        self.viewInbody.gooff()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
